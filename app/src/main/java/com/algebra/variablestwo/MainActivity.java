package com.algebra.variablestwo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText etInput;
    private TextView tvResult;
    private Button bTestChar;
    private Button bTestString;
    private Button bLogin;

    private final String PASSWORD = "abc123";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initWidgets();
        setupListeners();
    }

    private void initWidgets() {
        etInput = findViewById(R.id.etInput);
        tvResult = findViewById(R.id.tvDisply);
        bTestChar = findViewById(R.id.bChar);
        bTestString = findViewById(R.id.bString);
        bLogin = findViewById(R.id.bLogin);
    }

    private void setupListeners() {

        bTestString.setOnClickListener(view -> {
            String s = getText();
            printResult(s);
            printResult(s + " hello");
            printResult(s + 5);
            printResult("Upper case: " + s.toUpperCase());
            printResult("Lower case: " + s.toLowerCase());
            printResult("Replace: " + s.replace('a', 'b'));
            printResult("Concat: " + s.concat(" some other text"));
            printResult("Substring: " + s.substring(1));
            printResult("Substring: " + s.substring(0, 3));
            printResult(s + " trimmed " + s.trim());
            printResult("Equals algebra: " + s.equals("algebra"));
            printResult("Contains a: " + s.contains("a"));
            printResult("Ends with a: " + s.endsWith("a"));
            printResult("Ends with A: " + s.endsWith("A"));
            printResult("Starts with a: " + s.startsWith("a"));
            printResult("Starts with A: " + s.startsWith("A"));
            printResult("Compare to A: " + s.compareTo("A"));
            printResult("Contains a: " + s.contains("a"));

            String mojString = "Ivan";
            printResult(mojString.toLowerCase());
            printResult(mojString.toLowerCase());
            printResult(mojString.replace(mojString.charAt(0), 'b'));
            printResult(mojString.concat(" Bosnjakovic"));
        });

        bTestChar.setOnClickListener(view -> {
            String s = getText();
            char first = s.charAt(0);
            char second = s.charAt(1);

            char c = 'y';

            //Toast.makeText(this, "first: " + first + ", second: " + second, Toast.LENGTH_LONG).show();
            //ASCI table
            Toast.makeText(this, "first: " + (int) first + ", second: " + (int) second, Toast.LENGTH_LONG).show();
        });

        bLogin.setOnClickListener(view -> {
            String text = getText();
            Toast.makeText(this, "Password matches: " + text.equals(PASSWORD), Toast.LENGTH_LONG).show();
            //Toast.makeText(this, "Password matches: " + text.equalsIgnoreCase(PASSWORD), Toast.LENGTH_LONG).show();
            //Toast.makeText(this, "Password matches: " + text.toLowerCase().equals(PASSWORD.toLowerCase()), Toast.LENGTH_LONG).show();
        });

    }

    private String getText() {
        return etInput.getText().toString();
    }

    private void printResult(String result) {
        tvResult.append(result + "\n");
    }

}
